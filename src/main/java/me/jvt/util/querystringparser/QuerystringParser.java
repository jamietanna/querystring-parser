package me.jvt.util.querystringparser;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class QuerystringParser {
  public Map<String, String> parse(URI url) {
    Map<String, String> map = new LinkedHashMap<>();
    String query = url.getQuery();
    if (null == query || query.isEmpty()) {
      return map;
    }

    String[] pairs = query.split("&");
    for (String pair : pairs) {
      int idx = pair.indexOf("=");
      try {
        map.put(
            URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
            URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
      } catch (UnsupportedEncodingException e) {
        throw new IllegalStateException(e);
      }
    }
    return map;
  }
}
